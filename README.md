# Webcam Streamer

## Use

> $ go mod init \<pkg-name>
>
> $ go mod tidy
>
> $ go run main.go \<videoID> \<ip:port>

Optional:

> $ go build
>
> $ ./\<pkg-name> \<videoID> \<ip:port>
